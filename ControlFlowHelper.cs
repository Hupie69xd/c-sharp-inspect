﻿// Copyright (C) 2024  Hupie. Available under GPL 3.0 see LICENSE for details.
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.CodeAnalysis.Operations;


namespace CodeMemes;

internal static class ControlFlowHelper
{
    public static List<string> getCalledFunctions(ControlFlowGraph? graph)
    {
        var list = new List<string>();
        if (graph == null)
        {
            return list;
        }
        foreach (var block in graph.Blocks)
        {
            foreach (var op in block.Operations)
            {
                getInvocationTarget(op, list, graph);
            }
            getInvocationTarget(block.BranchValue, list, graph);
        }
        return list;
    }

    public static void getInvocationTarget(IOperation? inv, List<string> output, ControlFlowGraph graph)
    {
        if (inv == null)
        {
            return;
        }
        else if (inv.ChildOperations.Count != 0)
        {
            foreach (var op in inv.ChildOperations)
            {
                getInvocationTarget(op, output, graph);
            }
        }
        if (inv is IInvocationOperation inv2)
        {
            var func = inv2.TargetMethod.ToString();
            if (!string.IsNullOrEmpty(func))
            {
                output.Add(func);
            }
        }
        else if (inv is IFlowAnonymousFunctionOperation lambda)
        {
            var func = graph.GetAnonymousFunctionControlFlowGraph(lambda);
            output.AddRange(getCalledFunctions(func));
        }
    }
}
