﻿// Copyright (C) 2024  Hupie. Available under GPL 3.0 see LICENSE for details.
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis;


namespace CodeMemes;

internal static class Helper
{
    public static string join(string? s1, string s2)
    {
        if (string.IsNullOrEmpty(s1))
            return s2;
        else
            return s1 + "." + s2;
    }

    public static string getMethodWithArguments(MethodDeclarationSyntax node)
    {
        var ident = node.Identifier.ToString() + "(";
        var first = true;
        foreach (var param in node.ParameterList.Parameters)
        {
            if (!first)
                ident += ", ";

            ident += param.Type.ToString();
            first = false;
        }
        ident += ")";
        return ident;
    }
    public static string getName(SyntaxNode? node)
    {
        if (node == null || node is CompilationUnitSyntax)
        {
            return string.Empty;
        }
        if (node is ClassDeclarationSyntax clsNode)
        {
            return getName(clsNode);
        }
        else if (node is NamespaceDeclarationSyntax nsNode)
        {
            return getName(nsNode);
        }
        else if (node is InterfaceDeclarationSyntax iNode)
        {
            return getName(iNode);
        }
        else if (node is FileScopedNamespaceDeclarationSyntax fsnNode)
        {
            return getName(fsnNode);
        }
        else
        {
            throw new NotImplementedException($"Unknown syntax node of type: {node.GetType().FullName}");
        }
    }

    public static string getName(ClassDeclarationSyntax? node)
    {
        if (node == null)
        {
            return string.Empty;
        }
        return join(getName(node.Parent), node.Identifier.ToString());
    }

    public static string getName(NamespaceDeclarationSyntax? node)
    {
        if (node == null)
        {
            return string.Empty;
        }
        return join(getName(node.Parent), node.Name.ToString());
    }
    public static string getName(InterfaceDeclarationSyntax? node)
    {
        if (node == null)
        {
            return string.Empty;
        }
        return join(getName(node.Parent), node.Identifier.ToString());
    }
    public static string getName(FileScopedNamespaceDeclarationSyntax? node)
    {
        if (node == null)
        {
            return string.Empty;
        }
        return join(getName(node.Parent), node.Name.ToString());
    }
}
