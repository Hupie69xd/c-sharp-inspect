﻿// Based on code from https://stackoverflow.com/a/54551613/18908021 licenced under CC BY-SA 4.0.
// Modifications made by Hupie (c) 2024, available under the CC BY-SA 4.0 licence
// for terms see https://creativecommons.org/licenses/by-sa/4.0/
using CodeMemes;
using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.FlowAnalysis;
using Microsoft.CodeAnalysis.MSBuild;


if (args.Length < 2 || args.Length > 3)
{
    Console.WriteLine("Usage: codememes <path_to_solution> <project_in_solution> [output_json_file]");
    return;
}
string targetPath = args[0];
string project = args[1];
FileOutputHelper? fileHelper = null;
if (args.Length > 2)
{
    string outputFile = args[2];
    fileHelper = new FileOutputHelper(outputFile);
}


var options = CSharpParseOptions.Default
    .WithFeatures(new[] { new KeyValuePair<string, string>("flow-analysis", "") });

MSBuildLocator.RegisterDefaults();

var workspace = MSBuildWorkspace.Create();


var sln = await workspace.OpenSolutionAsync(targetPath);

ProjectDependencyGraph projectGraph = sln.GetProjectDependencyGraph();
var assemblies = new Dictionary<string, Stream>();

var projects = projectGraph.GetTopologicallySortedProjects().ToDictionary(
    p => p,
    p => sln.GetProject(p)?.Name);

var bllProjectId = projects.First(p => p.Value == project).Key; // choose project for analysis
var projectId = bllProjectId;
sln = sln.WithProjectParseOptions(projectId, options);

if (projectId == null)
{
    Console.WriteLine("Project id was not found, make sure your project is part of the solution");
    return;
    // handle null
}
var compilation = await sln.GetProject(projectId).GetCompilationAsync();


if (compilation == null)
{
    Console.WriteLine("Could not get the compilation for the selected project");
    return;
}
else if (string.IsNullOrEmpty(compilation.AssemblyName))
{
    Console.WriteLine("Could not find the assembly for the selected project");
    return;
}

foreach (var syntaxTree in compilation.SyntaxTrees)
{
    var methodNodes = from methodDeclaration in syntaxTree.GetRoot().DescendantNodes()
    .Where(x => x is MethodDeclarationSyntax)
                      select methodDeclaration;

    string classPath;
    try
    {
        classPath = Helper.getName(
            ((MethodDeclarationSyntax)methodNodes.First()).Parent);
    }
    catch (InvalidOperationException)
    {
        continue;
    }
    foreach (MethodDeclarationSyntax node in methodNodes)
    {
        var model = compilation.GetSemanticModel(node.SyntaxTree);
        var ident = Helper.getMethodWithArguments(node);
        if (node.SyntaxTree.Options.Features.Any())
        {
            var graph = ControlFlowGraph.Create(node, model); // CFG is here
            var fullyQualifiedFunctionName = classPath + "." + ident;
            var funcs = ControlFlowHelper.getCalledFunctions(graph);
            if (fileHelper != null)
            {
                fileHelper.write(fullyQualifiedFunctionName, funcs);
                continue;
            }
            if (funcs.Count == 0)
                Console.WriteLine(fullyQualifiedFunctionName + " call no other functions.");
            else
            {
                Console.WriteLine(fullyQualifiedFunctionName + " calls:");
                foreach (var func in funcs)
                {
                    Console.WriteLine($"{func}");
                }
                Console.WriteLine();
            }
        }
    }
}
fileHelper?.Dispose();
