﻿// Copyright (C) 2024  Hupie. Available under GPL 3.0 see LICENSE for details.
using System.Text;

namespace CodeMemes;

internal class FileOutputHelper : IDisposable
{
    public FileOutputHelper(string filename)
    {
        file = File.Create(filename);
        encoder = new UTF8Encoding();
        write("{\n");
    }

    public void write(string functionName, List<string> values)
    {
        if (!firstFunction)
        {
            write(",\n");
        }
        firstFunction = false;

        write($"  \"{functionName}\": [");
        bool first = true;
        foreach (var value in values)
        {
            if (!first)
            {
                write(",");
            }
            first = false;
            write($"\n    \"{value}\"");
        }
        if (first)
            write("]");
        else
            write("\n  ]");
    }

    private void write(string value)
    {
        var data = encoder.GetBytes(value);
        file.Write(data, 0, data.Length);
    }

    public void Dispose()
    {
        write("}");
        file.Flush();
        file.Dispose();
    }

    private FileStream file;
    private UTF8Encoding encoder;
    private bool firstFunction = true;
}
