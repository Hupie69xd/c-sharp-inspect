# Copyright (C) 2024  Hupie. Available under GPL 3.0 see LICENSE for details.
import json


def read_file(fname, mode='r'):
    if 'w' in mode:
        raise ValueError("Write flag in mode.")
    coding = None if 'b' in mode else "utf-8"
    
    with open(fname, mode, encoding=coding) as file:
        data = file.read()
    return data

def all_functions_in_calltree(func, func_map) -> [str]:
    funcs = [f for f, _ in iterator(func, func_map)]
    return list(dict.fromkeys(funcs))


def iterator(start_func, func_map):
    stack = [start_func]
    return _iterator(func_map, stack)

def _iterator(fm, st):
    curr = st[-1]
    yield curr, st
    fs = fm.get(curr)
    if fs is None:
        st.pop()
        return
    for f in fs:
        st.append(f)
        yield from _iterator(fm, st)
    st.pop()
    return


def path_to(from_function, to_function, func_map):
    stack = [from_function]
    if _path_to(to_function, func_map, stack):
        print("Path found:")
        for f in stack:
            print(f"-> {f}")
        print()
    else:
        print("Path between functions not found")

def path_to(from_function, to_function, func_map):
    for f, st in iterator(from_function, func_map):
        if f == to_function:
            break
    else:
        print("Path between functions not found")
        return
    
    print("Path found:")
    for f in st:
        print(f"-> {f}")
    print()

def dead_functions(*func_maps):
    global calls
    calls = {}
    for fm in func_maps:
        for func in fm:
            calls[func] = calls.get(func, 0) # not a call so dont increment
            for call in fm[func]:
                calls[call] = calls.get(call, 0) + 1
    dead_funcs = []
    for func in calls:
        if calls[func] == 0:
            dead_funcs.append(func)
    return dead_funcs
